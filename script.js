//Variabelen

const inputEl = document.querySelector("input"),
      specColorEl = document.querySelector(".color"),
      specSizeEl = document.querySelector(".size"),
      plusButtonEl = document.querySelector(".button_add"),
      minButtonEl = document.querySelector(".button_min"),
      selectColorEl = document.querySelector(".color_selector"),
      selectSizeEl = document.querySelector(".size_selector"),
      imgBigEl = document.querySelector(".img_Big"),
      imgSmallEl = document.querySelector(".img_Small"),
      moneyEl = document.querySelector(".money"),
      purchaseButtonEl = document.querySelector(".button_purchase");

let voorraad = 10;

//EventListeners

plusButtonEl.addEventListener('click', plusButtonClick);
minButtonEl.addEventListener('click', minButtonClick);
purchaseButtonEl.addEventListener('click', purchaseButtonClick);
selectColorEl.addEventListener('change', colorSwitch);
selectSizeEl.addEventListener('change', sizeSwitch);

//Functies

function plusButtonClick() {
  if (inputEl.value > voorraad - 1) {
    console.log("mag niet");
  } else {
    inputEl.value++; 
  }
};

function minButtonClick() {
  if (inputEl.value > 1 ) {
    inputEl.value--;
  } 
};

// Functie rekent ook al met het geld waar de producten voor worden verkocht.

function purchaseButtonClick() {
  color = selectColorEl.value;
  amount = inputEl.value;
  size = selectSizeEl.value;
  
  
  if (selectSizeEl.value === "Small") {
    prijs = 30;
  } 
  else if (selectSizeEl.value === "Medium") {
    prijs = 35;        
  }
  else if (selectSizeEl.value === "Large") {
    prijs = 40;
  }
  
  totalePrijs = amount * prijs;
  
  console.log("Het volgende is toegevoegd in het winkelmandje: " + amount + "x " + "met de grootte " + size + " in de kleur " + color);
  
  document.querySelector('.msg_addCart').innerHTML = "Producten toegevoegd aan je winkelmandje!";
  document.querySelector('.stock').innerHTML = voorraad + " op voorraad";
  inputEl.value = 1;
  show();
  timer();
}

function colorSwitch() {
  
  if (selectColorEl.value === "Lichtblauw") {
    imgBigEl.src = "https://images.wehkamp.nl/i/wehkamp/681284_pb_01/curver-dieren-ligbed-blauw-7290106931640.jpg?w=966";
    imgSmallEl.src = "https://images.wehkamp.nl/i/wehkamp/681284_pb_01/curver-dieren-ligbed-blauw-7290106931640.jpg?w=80&h=80";
    inputEl.value = 1;
    specColorEl.innerHTML = "LichtBlauw";
  }
  else if (selectColorEl.value === "Creme") {
    imgBigEl.src = "https://images.wehkamp.nl/i/wehkamp/681295_pb_01/curver-dieren-ligbed-beige-7290106931633.jpg?w=966";
    imgSmallEl.src = "https://images.wehkamp.nl/i/wehkamp/681295_pb_01/curver-dieren-ligbed-beige-7290106931633.jpg?w=80&h=80";
    inputEl.value = 1;
    specColorEl.innerHTML = "Creme";
    
  }
  
}

function sizeSwitch() {
  if (selectSizeEl.value === "Small") {
    moneyEl.innerHTML = "€30.00";
    specSizeEl.innerHTML = "70 x 50cm"
  } 
  else if (selectSizeEl.value === "Medium") {
    moneyEl.innerHTML = "€35.00";
    specSizeEl.innerHTML = "90 x 70cm"        
  }
  else if (selectSizeEl.value === "Large") {
    moneyEl.innerHTML = "€40.00";
    specSizeEl.innerHTML = "115 x 90cm"
  }
}

function show() {
  document.querySelector(".popup").style = "display: block;"
}

function hide() {
  document.querySelector(".popup").style = "display: none;"
}

function timer() {
  setTimeout(hide, 3000);
}






